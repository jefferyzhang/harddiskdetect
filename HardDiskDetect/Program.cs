﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Principal;
using System.Security.AccessControl;
using System.Text.RegularExpressions;

namespace HardDiskDetect
{
	class MainClass
	{
		static String _TAG = "HDSESDetect";
		public static void logIt(String msg)
		{
			String ss = String.Format("[{0}]{1}", _TAG, msg);
			Trace.WriteLine(ss);
			Console.WriteLine(ss);
		}

		//Dictionary<>
		static StringBuilder scsiret = new StringBuilder();
		public static Hashtable sClibration; 

		static void doDetect(System.Collections.Specialized.StringDictionary args, System.Threading.EventWaitHandle e){
			/// <summary>
			/// The internal process interface used to interface with the process.
			/// </summary>
//			ProcessInterface processInterace = new ProcessInterface();
//			processInterace.OnProcessOutput += new ProcessEventHanlder(processInterace_OnProcessOutput);
//			processInterace.OnProcessError += new ProcessEventHanlder(processInterace_OnProcessError);
//			processInterace.OnProcessInput += new ProcessEventHanlder(processInterace_OnProcessInput);
//			processInterace.OnProcessExit += new ProcessEventHanlder(processInterace_OnProcessExit);
			int nHardDriveCount = 0;
			//String sOldscsi="";
			int nInterval = 1000;
			int nFirst = 1;
			String[] sss = { "" };
			if (!args.ContainsKey ("interval") || !int.TryParse (args ["interval"], out nInterval))
				nInterval = 1000;
			while (!e.WaitOne(nFirst)) {
				nFirst = nInterval;
				do {
					logIt(DateTime.Now.ToLongTimeString());
					nHardDriveCount = 0;
					try{
						scsiret.Clear();
						int exitcode = 0;
						String[] ssret = HardDiskMap.runExe("lsscsi", "-s -g", out exitcode);
//						processInterace.StartProcess ("lsscsi", "-s");
//						while (processInterace.IsProcessRunning) {
//							//if (waithand.WaitOne(0)) break;
//							//processInterace.WriteInput("\x03\r\n");
//							Thread.Sleep (100);
//						}
						if(ssret.SequenceEqual(sss)){
						//if (String.Compare(sOldscsi, scsiret.ToString ())==0){
							//if (e.WaitOne(nInterval)) return ;
							Thread.Sleep(nInterval);
							continue;
						}

						//sOldscsi= scsiret.ToString();
						//String[] sss = sOldscsi.Split (new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
						sss = ssret;
						foreach (var s in sss) {
							HardDiskMap.AddHardDisk (s);
							nHardDriveCount++;
						}

						HardDiskMap.RemovHardDisk (sss);
					}catch(Exception){}
				} while(nHardDriveCount == 0);

				//HardDiskMap.runSg_map ();

				HardDiskMap.readDataFromLSI (args);

				lock (HardDiskMap.listHD) {
					foreach (var sinfo in HardDiskMap.listHD) {
						//Console.WriteLine (sinfo.ToString ());
						if (args.ContainsKey ("onlycard") && String.IsNullOrEmpty (sinfo.Value.sUILabel)) {
							Console.WriteLine ();
							continue;
						}
						Console.WriteLine (sinfo.Value.ToDetectString ());
					}
				}
			}

		}

		public static string EVENT_NAME = @"Global\LSI_SAS_READ_INFO_SERVER";

		static void startServer(System.Collections.Specialized.StringDictionary args)
		{
			bool own = false;
			try
			{
//				var users = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
//				var rule = new EventWaitHandleAccessRule(users, EventWaitHandleRights.FullControl, AccessControlType.Allow);
//				var security = new EventWaitHandleSecurity();
//				security.AddAccessRule(rule);

				EventWaitHandle e = new EventWaitHandle(false, EventResetMode.ManualReset, EVENT_NAME, out own);
				if (own)
				{
					//UDPListener.CreateUDPServer();
					TCPServer.CreateTCPServer();
					doDetect(args, e);
					e.Close();
				}
				else
				{
					logIt("Another instance is running.");
					e.Close();
				}
			}
			catch (Exception ex)
			{
				logIt(ex.Message);
				logIt(ex.StackTrace);
			}
		}

		public static int Main (string[] args)
		{
			sClibration = ConfigurationManager.GetSection ("mapcalibrationSection") as Hashtable;

			logIt(String.Format("{0} start: ++ version: {1}",
				Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName),
				Process.GetCurrentProcess().MainModule.FileVersionInfo.FileVersion));

			InstallContext myInstallContext = new InstallContext(null, args);
			if (myInstallContext.IsParameterTrue("debug"))
			{
				logIt("Wait any key to dubug, Please attach...\n");
				Console.ReadKey();
			}
			if(myInstallContext.IsParameterTrue("help"))
			{
				//Usage();
				return 0;
			}

			if (!myInstallContext.Parameters.ContainsKey ("adaptors")) {
				String cardinfo = ConfigurationManager.AppSettings["cards"];
				myInstallContext.Parameters.Add ("adaptors", cardinfo);
			} 

			if (myInstallContext.IsParameterTrue ("start-server")) {
				startServer (myInstallContext.Parameters);
			} else if(myInstallContext.IsParameterTrue("kill-server")) {
				logIt ("kill-server called.");
				UDPListener.SendCmd ("exit\n");
//				EventWaitHandle e = new EventWaitHandle(false, EventResetMode.ManualReset, EVENT_NAME);
//				if (e != null) {
//					e.Set ();
//					e.Close ();
//				}
			}else if(myInstallContext.IsParameterTrue("print")) {
				UDPListener.SendCmd ("print\n");
			}

			return 0;
		}

		private static void processInterace_OnProcessExit(object sender, ProcessEventArgs args)
		{
			Console.WriteLine("lsscsiExitCode="+args.Code);
		}

		private static void processInterace_OnProcessInput(object sender, ProcessEventArgs args)
		{
			//Console.WriteLine(args.Content);  
		}

		private static void processInterace_OnProcessError(object sender, ProcessEventArgs args)
		{
			Console.WriteLine(args.Content);
			//listscsi.Add (args.Content);
		}

		private static void processInterace_OnProcessOutput(object sender, ProcessEventArgs args)
		{
			//Console.WriteLine(args.Content);
			scsiret.Append (args.Content);
		}
	}
}