﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Security.Principal;
using System.Security.AccessControl;
using Newtonsoft.Json;

namespace HardDiskDetect
{
	public class UDPListener
	{
		private const int listenPort = 11000;
		static public void CreateUDPServer()
		{
			new Thread (Run).Start ();
		}

		static public void SendCmd(String sCmd){
			Socket sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
				ProtocolType.Udp);
			IPAddress send_to_address = IPAddress.Parse("127.0.0.1");
			IPEndPoint sending_end_point = new IPEndPoint(send_to_address, listenPort);
			// Set the timeout for synchronous receive methods to 
			// 1 second (1000 milliseconds.)
			sending_socket.ReceiveTimeout = 1000;

			byte[] send_buffer = Encoding.ASCII.GetBytes(sCmd);
			try
			{
				sending_socket.SendTo(send_buffer, sending_end_point);
				byte[] rcv_buffer = new byte[40960];
				IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
				EndPoint remote = (EndPoint)sender;
				int len = sending_socket.ReceiveFrom(rcv_buffer,ref remote);
				String received_data = Encoding.ASCII.GetString(rcv_buffer, 0, len);
				Console.WriteLine(received_data);
				sending_socket.Close();
			}
			catch (Exception send_exception )
			{
				Console.WriteLine(" Exception {0}", send_exception.Message);
			}

		}

		static void Run()
		{
			bool done = false;
			UdpClient listener = new UdpClient(listenPort);
			IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);
			string received_data;
			byte[] receive_byte_array;
			try
			{
				EventWaitHandle e = new EventWaitHandle(false, EventResetMode.ManualReset, MainClass.EVENT_NAME);
				while (!done)
				{
					if(e.WaitOne(0)) break;
					// this is the line of code that receives the broadcase message.
					// It calls the receive function from the object listener (class UdpClient)
					// It passes to listener the end point groupEP.
					// It puts the data from the broadcast message into the byte array
					// named received_byte_array.
					// I don't know why this uses the class UdpClient and IPEndPoint like this.
					// Contrast this with the talker code. It does not pass by reference.
					// Note that this is a synchronous or blocking call.

					receive_byte_array = listener.Receive(ref groupEP);
					received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
					Console.WriteLine(" from {0} data follows\n[{1}]", groupEP.ToString(), received_data);
					if(String.Compare(received_data, "exit\n", true)==0)
					{
						listener.Send(Encoding.ASCII.GetBytes("exit"),"exit".Length, groupEP);
//						var users = new SecurityIdentifier(WellKnownSidType.BuiltinUsersSid, null);
//						var rule = new EventWaitHandleAccessRule(users, EventWaitHandleRights.Synchronize | EventWaitHandleRights.Modify,
//							AccessControlType.Allow);
//						var security = new EventWaitHandleSecurity();
//						security.AddAccessRule(rule);
//						bool own;
						//EventWaitHandle e = new EventWaitHandle(false, EventResetMode.ManualReset, MainClass.EVENT_NAME);
						if (e != null)
						{
							MainClass.logIt ("kill-server run.");
							e.Set();
							e.Close();
							done = true;
						}
					}
					else if(String.Compare(received_data, "print\n", true)==0)
					{	
						//StringBuilder sb = new StringBuilder();
						string s;
						lock(HardDiskMap.listHD){
//							foreach (var sinfo in HardDiskMap.listHD) {
//								//Console.WriteLine (sinfo.ToString ());
//								if (String.IsNullOrEmpty (sinfo.Value.sUILabel)) {
//									Console.WriteLine ();
//									sb.AppendLine();
//									continue;
//								}
//								Console.WriteLine (sinfo.Value.ToDetectString ());
//								sb.AppendLine(sinfo.Value.ToDetectString());
//							}

							s = JsonConvert.SerializeObject(HardDiskMap.listHD);
							Console.WriteLine("length = "+s.Length);
							Console.WriteLine(s);
						}
						//listener.Send(Encoding.UTF8.GetBytes(sb.ToString()),sb.Length, groupEP);
						listener.Send(Encoding.UTF8.GetBytes(s),s.Length, groupEP);
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			listener.Close();
			return ;
		}
	} // end of class UDPListener
}

