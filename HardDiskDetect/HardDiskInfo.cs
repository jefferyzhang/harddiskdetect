﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Text;
using System.IO;

namespace HardDiskDetect
{
	public class HardDiskInfo
	{
		public String locpath;
		public String sType;
		public String sManufacture;
		public String sModel;
		public String sVersion;
		public String sLinuxName;
		public String sSize;
		public String sSGLibName;
		//use smartctl get info
		//public String model replace using this
		public String sSerialno;
		public String luwwndevId;
		public String sCalibration;
		public String sUILabel;
		//public String firware version replace by sVersion;
		public Dictionary<String, String> otherinfo = new Dictionary<String, String>(StringComparer.OrdinalIgnoreCase);

		override
		public String ToString()
		{
			StringBuilder sb = new StringBuilder ();
			sb.AppendLine ("LocPath=" + locpath);
			sb.AppendLine ("Type=" + sType);
			sb.AppendLine ("Manufacture=" + sManufacture);
			sb.AppendLine ("Model=" + sModel);
			sb.AppendLine ("Version=" + sVersion);
			sb.AppendLine ("Name=" + sLinuxName);
			sb.AppendLine ("SGName=" + sSGLibName);
			sb.AppendLine ("Size=" + sSize);
			sb.AppendLine ("SerialNo=" + sSerialno);
			sb.AppendLine ("Calibration=" + sCalibration);
			foreach (var ss in otherinfo) {
				sb.AppendFormat ("{0}={1}\n", ss.Key, ss.Value);
			}
			return sb.ToString ();
		}

		public String ToDetectString()
		{
			StringBuilder sb = new StringBuilder ();

			sb.AppendFormat("{0}={1}\n",sUILabel,sSGLibName);
			sb.AppendLine("SerialNo=" + sSerialno);
			sb.AppendLine ("Size=" + sSize);
			sb.AppendLine ("Type=" + sType);
			sb.AppendLine ("Manufacture=" + sManufacture);
			sb.AppendLine ("Model=" + sModel);
			sb.AppendLine ("Version=" + sVersion);
			sb.AppendLine ("Name=" + sLinuxName);
			sb.AppendLine ("SGName=" + sSGLibName);
			sb.AppendLine ("LocPath=" + locpath);
			sb.AppendLine ();
			return sb.ToString ();
		}
	}

	public class HardDiskMap
	{
		public static Dictionary<String, HardDiskInfo> listHD = new Dictionary<String, HardDiskInfo> ();
		//[8:0:1:0]    disk    ATA      SAMSUNG MZMPC128 201Q  /dev/sdb    128GB
		//[9:0:22:0]   disk    ATA      WDC WD800JD-75MS 1E04  /dev/sdb   /dev/sg2   80.0GB
		static public void AddHardDisk(String sscsi)
		{
			if (String.IsNullOrEmpty (sscsi))
				return;
			if (HardDiskMap.listHD.ContainsKey (sscsi)) {
				if (String.IsNullOrEmpty (listHD [sscsi].sUILabel) && HardDiskMap.listHD [sscsi].otherinfo.Count == 0) {
					if (!String.IsNullOrEmpty (listHD [sscsi].sLinuxName)) {
						readDataFromSmartCtl (listHD [sscsi].sLinuxName, listHD [sscsi]);
					}
				}
				return;
			}
			Match match = Regex.Match (sscsi, @"^([\s\S]{13})(disk[\s\S]{4})([\s\S]{9})([\s\S]{17})([\s\S]{6})([\s\S]{10})([\s\S]{10})([\s\S]{8})$", RegexOptions.IgnoreCase);
			if (match.Success) {
				HardDiskInfo hdinfo = new HardDiskInfo();
				hdinfo.locpath = match.Groups [1].Value.Trim();
				hdinfo.sType = match.Groups [2].Value.Trim();
				hdinfo.sManufacture = match.Groups [3].Value.Trim();
				hdinfo.sModel = match.Groups [4].Value.Trim();
				hdinfo.sVersion = match.Groups [5].Value.Trim();
				hdinfo.sLinuxName = match.Groups [6].Value.Trim();
				hdinfo.sSGLibName = match.Groups [7].Value.Trim ();
				hdinfo.sSize = match.Groups [8].Value.Trim();
				if (String.IsNullOrEmpty (hdinfo.sLinuxName)) {
					Console.WriteLine (sscsi);
					return;
				}
				lock (HardDiskMap.listHD) {
					listHD.Add (sscsi, hdinfo);
				}
				if (!String.IsNullOrEmpty (hdinfo.sLinuxName)) {
					readDataFromSmartCtl (hdinfo.sLinuxName, hdinfo);
				}
			}
		}

		static public int RemovHardDisk(String[] scsilist)
		{
			if (scsilist==null || scsilist.Length == 0)
				return 0;
			List<string> keyList = new List<string>(HardDiskMap.listHD.Keys);
			foreach (var s in scsilist) {
				if (keyList.Contains (s))
					keyList.Remove (s);
			}
			int ret = 0;

			lock (HardDiskMap.listHD) {
				foreach (var key in keyList) {
					HardDiskMap.listHD.Remove (key);
					ret++;
				}
			}
			return ret;
		}

		public static void runSg_map()
		{
			int err = 0;
			try{
			String[] lines = runExe("sg_map", "-sd", out err, 60 * 60 * 1000);
			foreach (String line in lines) {
				Match match = Regex.Match (line, @"^(/dev/sg\d+)[\s]*?(/dev/sd.*)$", RegexOptions.IgnoreCase);
				if (match.Success) {
					String sdname = match.Groups [2].Value.Trim();
					if (String.IsNullOrEmpty (sdname))
						continue;
					
					String sgname = match.Groups [1].Value.Trim();
					foreach (var hdinfo in listHD) {
						if (String.Compare (hdinfo.Value.sLinuxName, sdname, true) == 0) {
							hdinfo.Value.sSGLibName = sgname;
							if (String.IsNullOrEmpty (hdinfo.Value.luwwndevId)) {
								readDataFromSmartCtl (hdinfo.Value.sLinuxName, hdinfo.Value);
							}
							break;
						}
					}
				}
			}
			}catch(Exception){
			}
		}

		static void readDataFromSmartCtl(String sDevName, HardDiskInfo hdinfo)
		{
			// runexe "smartctl /dev/sdc -i"
			//[8:0:8:0]    disk    SEAGATE  STT30065 CLAR300 E50A  /dev/sdd        -
			int err = 0;
			string[] ingoreKeys = {"Device is", "Local Time is","SMART support is" ,"Warning"};
			Console.WriteLine (sDevName);
			if (!sDevName.StartsWith ("/dev/"))
				return;

			Boolean bOKDisk = false;
			String[] lines = runExe("smartctl", string.Format("{0} -i -H", sDevName), out err, 60 * 1000);
			lock (HardDiskMap.listHD) {
				foreach (String line in lines) {
					int pos = line.IndexOf (':');
					if (pos > 0) {
						string k = line.Substring (0, pos).Trim ();
						string v = line.Substring (pos + 1).Trim ();
						if (String.IsNullOrEmpty (k) || String.IsNullOrEmpty (v))
							continue;
						if (Array.IndexOf (ingoreKeys, k) > -1) {
							continue;
						} else if (string.Compare ("Device Model", k, true) == 0) {
							hdinfo.sModel = v;
						} else if (string.Compare ("Serial Number", k, true) == 0) {
							hdinfo.sSerialno = v;
						} else if (string.Compare ("LU WWN Device Id", k, true) == 0) {
							hdinfo.luwwndevId = v.Replace (" ", "");
						} else if (string.Compare ("Firmware Version", k, true) == 0) {
							hdinfo.sVersion = v;
						} else if (string.Compare ("Logical Unit id", k, true) == 0) {
							hdinfo.otherinfo ["LogicalUnitID"] = v.Replace ("-", "").Replace ("0x", "");
						} else if (string.Compare (hdinfo.sSize, "-") == 0 && string.Compare ("User Capacity", k, true) == 0) {
							Match match = Regex.Match (v, @"^.*?\[(.*?)\]$", RegexOptions.IgnoreCase); 
							if (match.Success)
								hdinfo.sSize = match.Groups [1].Value.Replace (" ", "");
						} else {
							if (string.Compare (k, "SMART Status command failed", true) == 0) {
								if (hdinfo.otherinfo.ContainsKey ("HD-health")) {
									hdinfo.otherinfo ["HD-health"] = v;
								} else {
									hdinfo.otherinfo.Add ("HD-health", v);
								}
							} else if (String.Compare (k, "SMART overall-health self-assessment test result", true) == 0 ||
							         String.Compare (k, "SMART Health Status", true) == 0||
									String.Compare(k,"Read SMART Data failed", true)==0) {
								k = "HD-health";
								if (!hdinfo.otherinfo.ContainsKey ("HD-health")) {
									hdinfo.otherinfo.Add ("HD-health", v);
								}
							}
							else if (hdinfo.otherinfo.ContainsKey (k)) {
								hdinfo.otherinfo [k] = v;
							} else {
								hdinfo.otherinfo.Add (k, v);
							}
						}
					} else {
						/*
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x000f   166   166   006    Pre-fail  Always       -       0
  5 Reallocated_Sector_Ct   0x0032   253   253   036    Old_age   Always       -       0
  9 Power_On_Hours          0x0032   099   099   000    Old_age   Always       -       1073
 12 Power_Cycle_Count       0x0032   100   100   020    Old_age   Always       -       64
100 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       121414
					*/
						Match match = Regex.Match (line, @"^\s*(\d+)\s+(\S+)\s+(0x[0-9a-fA-F]{4})\s+(\d+)\s+(\d+)\s+(\d+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s*", RegexOptions.IgnoreCase);
						if (match.Success) {
							String ATTRIBUTE_NAME = match.Groups [2].Value;
							String RAW_VALUE = match.Groups [10].Value;
							if (hdinfo.otherinfo.ContainsKey (ATTRIBUTE_NAME)) {
								hdinfo.otherinfo [ATTRIBUTE_NAME] = RAW_VALUE;
							} else {
								hdinfo.otherinfo.Add (ATTRIBUTE_NAME, RAW_VALUE);
							}
						}
					}
				}
				if (!hdinfo.otherinfo.ContainsKey ("HD-health")) {
					hdinfo.otherinfo.Clear ();
					return;
				} else {
					bOKDisk = string.Compare (hdinfo.otherinfo ["HD-health"], "OK", true) == 0 || hdinfo.otherinfo ["HD-health"].StartsWith ("PASS", StringComparison.CurrentCultureIgnoreCase);
				}
			}

			if (bOKDisk) {
				lines = runExe("smartctl", string.Format("{0} -A", sDevName), out err, 60 * 1000);
				lock (HardDiskMap.listHD) {
					int badsectors = 0;
					foreach (String line in lines) {
						int pos = line.IndexOf (':');
						if (pos > 0) {
							string k = line.Substring (0, pos).Trim ();
							string v = line.Substring (pos + 1).Trim ();
							if (String.IsNullOrEmpty (k) || String.IsNullOrEmpty (v))
								continue;
							if (string.Compare ("Elements in grown defect list", k, true) == 0) {
								hdinfo.otherinfo ["badsectors"] = v;
							} else if (string.Compare ("Non-medium error count", k, true) == 0) {
								hdinfo.otherinfo ["Nonmediumerrorcnt"] = v;
							} 
						} else {
							Match match = Regex.Match (line, @"^\s*(\d+)\s+(\S+)\s+(0x[0-9a-fA-F]{4})\s+(\d+)\s+(\d+)\s+(\d+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s*", RegexOptions.IgnoreCase);
							if (match.Success) {
								String ATTRIBUTE_NAME = match.Groups [2].Value;
								String RAW_VALUE = match.Groups [10].Value;
								if (hdinfo.otherinfo.ContainsKey (ATTRIBUTE_NAME)) {
									hdinfo.otherinfo [ATTRIBUTE_NAME] = RAW_VALUE;
								} else {
									hdinfo.otherinfo.Add (ATTRIBUTE_NAME, RAW_VALUE);
								}
								if (string.Compare ("Reallocated_Sector_Ct", ATTRIBUTE_NAME, true) == 0 ||
									string.Compare ("Current_Pending_Sector", ATTRIBUTE_NAME, true) == 0) {
									int aa = 0;
									if (int.TryParse (RAW_VALUE.Trim(), out aa)) {
										badsectors += aa;
									}
									if (hdinfo.otherinfo.ContainsKey ("badsectors")) {
										hdinfo.otherinfo ["badsectors"] = badsectors.ToString();
									} else {
										hdinfo.otherinfo.Add ("badsectors", badsectors.ToString());
									}
								}
							}
						}
					}

				}
			}

		}

		public static void readDataFromLSI(System.Collections.Specialized.StringDictionary args)
		{
			// runexe "sas2ircu.exe 0 display"
			if (args.ContainsKey("adaptors"))
			{
				string[] adaptors = args["adaptors"].Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
				foreach (string s in adaptors){
					try{
						int err = 0;
						String sExeFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "sas2ircu");
						if(!File.Exists(sExeFileName)) sExeFileName = "sas2ircu";
						string[] result = runExe(sExeFileName, string.Format("{0} DISPLAY", s), out err, 60 * 60 * 1000);
						Dictionary<String, String>[] props = parseLsiData(result);
						Console.WriteLine(String.Format("lsidata props count = {0}", props.Length));
						foreach (var prop in props) {
							lock (HardDiskMap.listHD) {
								foreach(var hdval in listHD){
									var hd = hdval.Value;
		//								Console.WriteLine(String.Format("hd serial no={0}", hd.sSerialno));
									if (String.IsNullOrEmpty (hd.sSerialno)){
										continue;
									}
									//this sas3ircu
									//if ((prop.ContainsKey ("Serial") && String.Compare (prop ["Serial"], hd.sSerialno, true) == 0) ||
									//   (prop.ContainsKey ("SerialVPD") && String.Compare (prop ["SerialVPD"], hd.sSerialno, true) == 0)) {
									String sSerial = hd.sSerialno;
									sSerial = sSerial.Replace ("-", "");
									//Console.WriteLine(String.Format("sSerial={0},Serail={1}",sSerial, prop.ContainsKey("Serial")?prop ["Serial"]:""));
									String sLuID=hd.otherinfo.ContainsKey("LogicalUnitID") ? hd.otherinfo["LogicalUnitID"]:"";
									if ((prop.ContainsKey("Serial") && (hd.sSerialno.StartsWith(prop ["Serial"])||sSerial.StartsWith(prop ["Serial"])))||
										(prop.ContainsKey("GUID") && (String.Compare(prop["GUID"],hd.luwwndevId)==0 || String.Compare(prop["GUID"],sLuID)==0))){								hd.sCalibration = String.Format ("{0}_{1}", s, prop.ContainsKey("Slot")?prop["Slot"]:"");
										hd.sUILabel = MainClass.sClibration.ContainsKey (hd.sCalibration) ? MainClass.sClibration [hd.sCalibration].ToString() : "";
										//Console.WriteLine (hd.sUILabel);
										foreach (var pr in prop) {
											try{
												hd.otherinfo.Add(pr.Key.ToString(), pr.Value.ToString());
											}catch(Exception){
											}
										}
										break;
									}
								}
							}
						}
					}catch(Exception){
					}
				}
			}
		}

		static Dictionary<String, String>[] parseLsiData(string[] lines)
		{
			MainClass.logIt("parseLsiData: ++");
			//List<System.Collections.Specialized.StringDictionary> diskinfos = new List<System.Collections.Specialized.StringDictionary>();
			//System.Collections.Specialized.StringDictionary map = new System.Collections.Specialized.StringDictionary();
			List<Dictionary<String, String>> diskinfos = new List<Dictionary<String, String>>();
			Dictionary<String, String> map = new Dictionary<String, String>(StringComparer.OrdinalIgnoreCase);
			map.Add("Enclosure #", "Enclosure");
			map.Add("Slot #", "Slot");
			map.Add("State", "State");
			map.Add("SAS Address", "SASAddress");
			map.Add(@"Size (in MB)/(in sectors)", "Size");
			map.Add("Manufacturer", "Manufacturer");
			map.Add("Model Number", "Model");
			map.Add("Firmware Revision", "Firmware");
			map.Add("Serial No", "Serial");
			map.Add("Unit Serial No(VPD)", "SerialVPD");
			map.Add("GUID", "GUID");
			map.Add("Protocol", "Protocol");
			map.Add("Drive Type", "DriveType");
			List<StringBuilder> disks = new List<StringBuilder>();
			StringBuilder current = null;
			foreach (string line in lines)
			{
				if (string.Compare("Device is a Hard disk", line, true) == 0)
				{
					if (current != null)
						disks.Add(current);
					current = new StringBuilder();
				}
				else if (string.Compare("Enclosure information", line, true) == 0)
				{
					if (current != null)
						disks.Add(current);
					current = null;
					break;
				}
				if (current!=null)
				{
					current.AppendLine(line);
				}
			}
			foreach (StringBuilder sb in disks)
			{
				//System.Collections.Specialized.StringDictionary prop = new System.Collections.Specialized.StringDictionary();
				Dictionary<String, String> prop = new Dictionary<String, String>();
				string[] infos = sb.ToString().Split(System.Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
				foreach (string line in infos)
				{
					int pos = line.IndexOf(':');
					if (pos > 0)
					{
						string k = line.Substring(0, pos).Trim();
						string v = line.Substring(pos+1).Trim();
						if (map.ContainsKey(k))
						{
							if (string.Compare("Size", map[k], true) == 0)
							{
								pos = v.IndexOf('/');
								if (pos > 0)
								{
									prop[map[k]] = v.Substring(pos + 1);
								}
							}
							else
								prop[map[k]] = v;
						}
					}
				}
				diskinfos.Add(prop);

			}
			MainClass.logIt("parseLsiData: --");
			return diskinfos.ToArray();
		}

		public static String[] runExe(string exeFilename, string param, out int exitCode, int timeout = 60*1000)
		{
			List<string> ret = new List<string>();
			exitCode = 1;
			MainClass.logIt(string.Format("[runExe]: ++ exe={0}, param={1}", exeFilename, param));
			try
			{
				if (!String.IsNullOrEmpty(exeFilename))
				{
					DateTime last_output = DateTime.Now;
					DateTime _start = DateTime.Now;
					System.Threading.ManualResetEvent ev = new System.Threading.ManualResetEvent(false);
					Process p = new Process();
					p.StartInfo.FileName = exeFilename;
					p.StartInfo.Arguments = param;
					p.StartInfo.UseShellExecute = false;
					p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
					p.StartInfo.RedirectStandardOutput = true;
					p.StartInfo.CreateNoWindow = true;
					//p.EnableRaisingEvents = true;
					p.OutputDataReceived += (obj, args) =>
					{
						last_output = DateTime.Now;
						if (!string.IsNullOrEmpty(args.Data))
						{
							MainClass.logIt(string.Format("[runExe]: {0}", args.Data));
							ret.Add(args.Data);
						}
						if (args.Data == null)
							ev.Set();
					};
					//p.Exited += (o, e) => { ev.Set(); };
					p.Start();
					_start = DateTime.Now;
					p.BeginOutputReadLine();
					bool process_terminated = false;
					bool proces_stdout_cloded = false;
					bool proces_has_killed = false;
					while (!proces_stdout_cloded || !process_terminated)
					{
						if (p.HasExited)
						{
							// process is terminated
							process_terminated = true;
							MainClass.logIt(string.Format("[runExe]: process is going to terminate."));
						}
						if (ev.WaitOne(1000))
						{
							// stdout is colsed
							proces_stdout_cloded = true;
							MainClass.logIt(string.Format("[runExe]: stdout pipe is going to close."));
						}
						if ((DateTime.Now - last_output).TotalMilliseconds > timeout)
						{
							MainClass.logIt(string.Format("[runExe]: there are {0} milliseconds no response. timeout?", timeout));
							// no output received within timeout milliseconds
							if (!p.HasExited)
							{
								exitCode = 1460;
								p.Kill();
								proces_has_killed = true;
								MainClass.logIt(string.Format("[runExe]: process is going to be killed due to timeout."));
							}
						}
					}
					if (!proces_has_killed)
						exitCode = p.ExitCode;
				}
			}
			catch (Exception ex)
			{
				MainClass.logIt(string.Format("[runExe]: {0}", ex.Message));
				MainClass.logIt(string.Format("[runExe]: {0}", ex.StackTrace));
			}
			MainClass.logIt(string.Format("[runExe]: -- ret={0}", exitCode));
			return ret.ToArray();
		}
	}
}

 