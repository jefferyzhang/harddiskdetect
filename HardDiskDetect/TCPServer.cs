﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Security.Principal;
using System.Security.AccessControl;
using Newtonsoft.Json;

namespace HardDiskDetect
{
	public class TCPServer
	{
		private const Int32 listenPort = 12000;

		static public void CreateTCPServer()
		{
			new Thread (Run).Start ();
		}

		static void SendCmd(String server, String message) 
		{
			try 
			{
				// Create a TcpClient.
				// Note, for this client to work you need to have a TcpServer 
				// connected to the same address as specified by the server, port
				// combination.
				TcpClient client = new TcpClient(server, listenPort);

				// Translate the passed message into ASCII and store it as a Byte array.
				Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);         

				// Get a client stream for reading and writing.
				//  Stream stream = client.GetStream();

				NetworkStream stream = client.GetStream();

				// Send the message to the connected TcpServer. 
				stream.Write(data, 0, data.Length);

				int messageSize = 81920;
				int readSoFar = 0;
				byte [] msg = new byte[messageSize];

				while(readSoFar < messageSize)
				{
					try{
						var read = stream.Read(msg, readSoFar, msg.Length - readSoFar);
						readSoFar += read;
						if(read==0)
							break;   // connection was broken
					}catch(Exception){}
				}				
				String responseData = System.Text.Encoding.ASCII.GetString(msg, 0, readSoFar);
				Console.WriteLine(responseData);

				// Close everything.
				stream.Close();         
				client.Close();         
			} 
			catch (ArgumentNullException e) 
			{
				Console.WriteLine("ArgumentNullException: {0}", e);
			} 
			catch (SocketException e) 
			{
				Console.WriteLine("SocketException: {0}", e);
			}
		}
		static void Run()
		{
			bool done = false;
			TcpListener server=null;   
			try
			{
				IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);
				server = new TcpListener(groupEP);
				EventWaitHandle e = new EventWaitHandle(false, EventResetMode.ManualReset, MainClass.EVENT_NAME);

				// Start listening for client requests.
				server.Start();

				// Buffer for reading data
				Byte[] bytes = new Byte[40960];
				String data = null;

				// Enter the listening loop.
				while(!done) 
				{
					if(e.WaitOne(0)) break;
					// Perform a blocking call to accept requests.
					// You could also user server.AcceptSocket() here.
					TcpClient client = server.AcceptTcpClient();            

					data = null;

					// Get a stream object for reading and writing
					NetworkStream stream = client.GetStream();

					int i;

					// Loop to receive all the data sent by the client.
					if((i = stream.Read(bytes, 0, bytes.Length))!=0) 
					{   
						// Translate data bytes to a ASCII string.
						data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
						Console.WriteLine(" from {0} data follows\n[{1}]", client.ToString(), data);
						if(String.Compare(data, "exit\n", true)==0)
						{
							stream.Write(Encoding.ASCII.GetBytes("exit"),0, "exit".Length);
							if (e != null)
							{
								MainClass.logIt ("kill-server run.");
								e.Set();
								e.Close();
								done = true;
							}
						}
						else if(String.Compare(data, "print\n", true)==0)
						{	
							//StringBuilder sb = new StringBuilder();
							string s;
							lock(HardDiskMap.listHD){
								s = JsonConvert.SerializeObject(HardDiskMap.listHD);
								Console.WriteLine("length = "+s.Length);
								Console.WriteLine(s);
							}
							stream.Write(Encoding.UTF8.GetBytes(s),0, s.Length);
						}
					}
					stream.Close();
					client.Close();
				}
			}
			catch(SocketException e)
			{
				Console.WriteLine("SocketException: {0}", e);
			}
			finally
			{
				// Stop listening for new clients.
				server.Stop();
			}

		}
	}
}

